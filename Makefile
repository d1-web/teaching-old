all:
	@echo "What do you want?  download or upload?  Better call with --dry-run to make sure it does what you think."
	@exit 1

warn:
	@echo "This is potentially VERY destructive.  Good luck."

download: warn
	rsync -av --delete mpii:/www/inf-resources-0/resources.mpi-inf.mpg.de/departments/d1/i mpii:/www/inf-resources-0/resources.mpi-inf.mpg.de/departments/d1/teaching_old.html .

upload: warn
	rsync -rv --delete i teaching_old.html mpii:/www/inf-resources-0/resources.mpi-inf.mpg.de/departments/d1
