      <!-- Note that there are still some files on this server from the previous installation.  Ask the sysadmin/webadmin if you need information from them. -->
      <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/" accesskey="1" title="D1: Algorithms and Complexity" id="d1-homepage">Algorithms and Complexity</a>
      <ul>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/people/" title="People working in Department 1: Algorithms &amp; Complexity" id="d1-people">People</a></li>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/research/" title="Research Areas - Department 1: Algorithms &amp; Complexity" id="d1-areas">Research Areas</a></li>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/offers/" title="Offers - Department 1: Algorithms &amp; Complexity" id="d1-offers">Offers</a></li>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/" title="Teaching - Department 1: Algorithms &amp; Complexity" id="d1-teaching">Teaching</a>
          <ul>
          <li><a href="/departments/d1/teaching_old.html" title="Teaching - Earlier Terms - Department 1: Algorithms &amp; Complexity" id="d1-teaching-old">Earlier Terms</a></li>
          </ul>
        </li>
	  <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/adfocs/" title="Max Planck Advanced Course on the Foundations of Computer Science" id="d1-adfocs">ADFOCS</a></li>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/talks-events/" title="Talks&nbsp;&amp;&nbsp;Events - Department 1: Algorithms &amp; Complexity" id="d1-talks">Talks&nbsp;&amp;&nbsp;Events</a></li>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/publications/" title="Publications - Department 1: Algorithms &amp; Complexity" id="d1-publications">Publications</a></li>
        <li><a href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/useful-links/" title="Useful Links - Department 1: Algorithms &amp; Complexity" id="d1-links">Useful Links</a></li>
      </ul>
      </li>
