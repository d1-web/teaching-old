<!-- This page is archived at https://github.mpi-klsb.mpg.de/d1-web/teaching-old -->

<h2>Winter 2015/2016 (Switch to Typo3)</h2>
<ul>
  <li>Lecture: <a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/algodat/" title="Opens internal link in current window">Algorithms and Data Structures</a>&nbsp;(<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~mhoefer/" title="Opens external link in new window">Martin Hoefer</a>, <a class="external-link-new-window" href="http://www-tcs.cs.uni-sb.de" title="Opens external link in new window">Raimund Seidel</a>)</li>
  <li>Lecture:&nbsp;<a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/approx/" title="Opens internal link in current window">Approximation Algorithms</a>&nbsp;(<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~gmayank/" title="Opens external link in new window">Mayank Goswami</a>, <a class="external-link-new-window" href="https://people.mpi-inf.mpg.de/~awiese/" title="Opens external link in new window">Andreas Wiese</a>)</li>
  <li>Lecture:&nbsp;<a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/tods/" title="Opens internal link in current window">Theory of Distributed Systems</a>&nbsp;(<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~clenzen/" title="Opens external link in new window">Christoph Lenzen</a>)</li>
  <li>Lecture: <a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/ideen/" title="Opens internal link in current window">Ideen und Konzepte der Informatik</a> (<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~mehlhorn/" title="Opens external link in new window">Kurt Mehlhorn</a>)</li>
  <li>Seminar:&nbsp;<a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/reading-group/" title="Opens internal link in current window">Reading Group Algorithms</a>&nbsp;(<a href="http://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>,&nbsp;<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~marvin/" title="Opens external link in new window">Marvin Künnemann</a>,&nbsp;<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~ruben/" title="Opens external link in new window">Ruben Becker</a>)</li>
  <li>Seminar:&nbsp;<a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/lower-bounds/" title="Opens internal link in current window">Algorithmic Lower Bound Techniques</a>&nbsp;(<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~parinya/" title="Opens external link in new window">Parinya Chalermsook</a>)</li>
  <li>Seminar: <a class="internal-link" href="https://www.mpi-inf.mpg.de/departments/algorithms-complexity/teaching/winter15/one/" title="Opens internal link in current window">Optimization and Entrepreneurship</a> (<a class="external-link-new-window" href="http://people.mpi-inf.mpg.de/~karrenba/" title="Opens external link in new window">Andreas Karrenbauer</a>)</li>
</ul>


<h2>Summer 2015</h2>
<ul>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss15/OPT/">Optimization</a> (<a href="http://people.mpi-inf.mpg.de/~karrenba/">Andreas Karrenbauer</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss15/GAC/">Graphs, Algorithms and Complexity</a> (<a href="http://people.mpi-inf.mpg.de/~parinya/">Parinya Chalermsook</a>, <a href="http://www.erikjanvl.nl">Erik Jan van Leeuwen</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss15/ParameterizedAlgorithms/">Parameterized Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~gphilip/">Geevarghese Philip</a>, <a href="http://www.erikjanvl.nl">Erik Jan van Leeuwen</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss15/ChipDesign/">Beyond classical chip design</a> (<a href="http://people.mpi-inf.mpg.de/~mfuegger/">Matthias Függer</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss15/AGT/">Algorithmic Game Theory</a> (<a href="http://people.mpi-inf.mpg.de/~xbei/">Xiaohui Bei</a>, <a href="http://people.mpi-inf.mpg.de/~tkesselh/">Thomas Kesselheim</a>)</li>
<li>Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss15/ReadingGroup/">Reading Group Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~mehlhorn/">Kurt Mehlhorn</a>, <a href="http://people.mpi-inf.mpg.de/~marvin/">Marvin Künnemann</a>, <a href="http://people.mpi-inf.mpg.de/~ruben/">Ruben Becker</a>)</li>
</ul>


<h2>Winter 2014/2015</h2>
<ul><li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws14/Ideen-der-Informatik/">Ideen der Informatik</a> (<a href="http://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws14/ToDS/">Distributed Computing</a> (<a href="http://people.mpi-inf.mpg.de/~clenzen/">Christoph Lenzen</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws14/ComputerAlgebra/">Computer Algebra</a> (<a href="http://people.mpi-inf.mpg.de/~msagralo/">Michael Sagraloff</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws14/AlgoDat/">Algorithms and Data Structures</a> (<a href="http://people.mpi-inf.mpg.de/~mhoefer/">Martin Hoefer</a>, <a href="http://people.mpi-inf.mpg.de/~mkerber/">Michael Kerber</a>)</li>
<li>Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws14/IntProg/">Integer Programming</a> (<a href="http://people.mpi-inf.mpg.de/~karrenba/">Andreas Karrenbauer</a>)</li>
<li>Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws14/ReadingGroup/">Reading Group Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="http://people.mpi-inf.mpg.de/~marvin/">Marvin Künnemann</a>, <a href="http://people.mpi-inf.mpg.de/~ruben/">Ruben Becker</a>)</li>
</ul>


<h2>Summer 2014</h2>
<ul><li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/OPT/">Optimization</a> (<a href="http://people.mpi-inf.mpg.de/%7Ekarrenba/" target="_blank">Andreas Karrenbauer</a>, <a href="http://people.mpi-inf.mpg.de/%7Eparinya/" target="_blank">Parinya Chalermsook</a>) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/DS/">Efficient Data Structures</a> (<a href="http://people.mpi-inf.mpg.de/~gawry/" target="_blank">Paweł Gawrychowski</a>, <a href="http://people.mpi-inf.mpg.de/~gmayank/" target="_blank">Mayank Goswami</a>, <a href="http://people.mpi-inf.mpg.de/~pnichols/" target="_blank">Patrick Nicholson</a>) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/ParameterizedAlgorithms/">Parameterized Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~erikjan" target="_blank">Erik Jan van Leeuwen</a>, <a href="http://people.mpi-inf.mpg.de/~gphilip/" target="_blank">Geevarghese Philip</a>) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/OnlineAlgos/">Online Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~aantonia/" target="_blank">Antonios Antoniadis</a>, <a href="http://people.mpi-inf.mpg.de/~mhoefer/" target="_blank">Martin Hoefer</a>) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/gitcs/">Great Ideas in Theoretical Computer Science</a> (<a href="http://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="http://people.mpi-inf.mpg.de/~hsun/" target="_blank">He Sun</a>)</li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/RandDiscStruct/">Random Discrete Structures</a> (<a href="http://people.mpi-inf.mpg.de/~kdutta/" target="_blank">Kunal Dutta</a>, Arjit Ghosh) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/ApproxAlgos/">Approximation Algorithms and Hardness of Approximation</a> (<a href="http://people.mpi-inf.mpg.de/~anna/" target="_blank">Anna Adamaszek</a>, <a href="http://people.mpi-inf.mpg.de/~awiese/" target="_blank">Andreas Wiese</a>) </li>
<li> Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/ReadingGroup/"> Reading Group Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="http://people.mpi-inf.mpg.de/~kbringma/" target="_blank">Karl Bringmann</a>) </li>
<li> Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss14/TopologicalOptimization/">Topological Optimization Problems</a> (<a href="http://people.mpi-inf.mpg.de/~madamasz/" target="_blank">Michal Adamaszek</a>, <a href="http://people.mpi-inf.mpg.de/~mkerber/" target="_blank">Michael Kerber</a>) </li></ul>

<h2>Winter 2013/2014</h2>
<ul><li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws13/ComputationalGeometry/">Computational Geometry</a> (<a href="http://people.mpi-inf.mpg.de/%7Eeric/" target="_blank">Eric Berberich</a>, <a href="http://people.mpi-inf.mpg.de/%7Emkerber/" target="_blank">Michael Kerber</a> )</li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws13/Approx/">Topics in Approximation Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/%7Ebsayan/" target="_blank">Sayan Bhattacharya</a>, <a href="http://people.mpi-inf.mpg.de/%7Eparinya/" target="_blank">Parinya Chalermsook</a>) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws13/ct/"> Ideen der Informatik</a> (<a href="http://people.mpi-inf.mpg.de/%7Emehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href=" http://people.mpi-inf.mpg.de/%7Eaneumann/" target="_blank">Adrian Neumann</a> ) </li>
<li> Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws13/beyondWorstCase/beyondWorstCase.html">Beyond Worst-Case Analysis</a> (<a href=" http://people.mpi-inf.mpg.de/%7Emhoefer/" target="_blank">Martin Hoefer</a>, <a href=" http://people.mpi-inf.mpg.de/%7Emarvin/" target="_blank">Marvin Künnenmann</a>) </li>
<li> Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws13/ReadingGroup/ReadingGroup.htm"> Reading Group Algorithms</a> (<a href=" http://people.mpi-inf.mpg.de/%7Emehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="http://people.mpi-inf.mpg.de/%7Ekbringma/" target="_blank">Karl Bringmann</a>) </li>
<li> Doctoral Privatissima: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws13/sgt">Spectral Graph Theory</a> (<a href="http://people.mpi-inf.mpg.de/%7Ehsun/" target="_blank">He Sun</a>)</li></ul>

<h2>Summer 2013</h2>
<ul><li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss13/OPT/">Optimization</a> (<a href="http://people.mpi-inf.mpg.de/~karrenba/">Andreas Karrenbauer</a>, <a href="http://people.mpi-inf.mpg.de/~mmnich/">Matthias Mnich</a>) </li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss13/strings/">Algorithms on Strings</a> (<a href="http://people.mpi-inf.mpg.de/~gawry/">Pawel Gawrychowski</a>)</li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss13/GraphTheory/">Graph Theory</a> (<a href="http://people.mpi-inf.mpg.de/~ajez/">Artur Jez</a>, <a href="http://people.mpi-inf.mpg.de/~jeschmid/">Jens M. Schmidt</a>)</li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss13/gitcs/">Great Ideas in Theoretical Computer Science</a> (<a href="http://people.mpi-inf.mpg.de/~hsun/">He Sun</a>, <a href="http://people.mpi-inf.mpg.de/~mehlhorn/">Kurt Mehlhorn</a>)</li>
<li> Lecture: <span style="text-decoration:line-through; ">Natural Algorithms</span> (<a href="http://people.mpi-inf.mpg.de/~mehlhorn/">Kurt Mehlhorn</a>) (cancelled)</li>
<li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss13/GameTheory/">Learning, Game Theory and Optimization</a> (<a href="http://people.mpi-inf.mpg.de/~Emhoefer/">Martin Hoefer</a>)</li>
<li> Seminar: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ss13/ReadingGroup/ReadingGroup.htm"> Reading Group Algorithms</a> (<a href="http://people.mpi-inf.mpg.de/~Emehlhorn/">Kurt Mehlhorn</a>, <a href="http://people.mpi-inf.mpg.de/~Ekbringma/">Karl Bringmann</a>) </li></ul>


<h2>Winter 2012/2013</h2>
<ul><li> Lecture:    <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/AGT/">Algorithmic Game Theory</a> (<a href="http://www.cs.duke.edu/~bsayan/">Sayan Bhattacharya</a>, <a href=" http://people.mpi-inf.mpg.de/~vanstee/">Rob van Stee</a>) </li>

  <li> Lecture:  <a href=" http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/ct/"> Ideen der Informatik</a> (<a href=" http://people.mpi-inf.mpg.de/~mehlhorn/">Kurt Mehlhorn</a>, <a href=" http://people.mpi-inf.mpg.de/~aneumann/">Adrian Neumann</a>) </li>

  <li> Lecture:  <a href=" http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/sublinear">Sublinear Algorithms</a> (<a href=" http://people.mpi-inf.mpg.de/~sauerwal/">Thomas Sauerwald</a>) </li>

  <li> Lecture: <a href=" http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/basicmath">Basic Mathematical Techniques for Computer Scientists</a> (<a href=" http://people.mpi-inf.mpg.de/~gphilip/">Geevarghese Philip"</a>)</li>

  <li> Lecture: <a href="http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/rmcs">Randomized Methods in Computer Science</a> (<a href=" http://people.mpi-inf.mpg.de/~doerr/">Benjamin Doerr</a>)</li>

  <li> Seminar: <a href=" http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/GraphsOnSurfaces">Graphs on Surfaces</a> (<a href=" http://people.mpi-inf.mpg.de/~mmnich/">Matthias Mnich</a>, <a href=" http://people.mpi-inf.mpg.de/~jeschmid/">Jens M. Schmidt</a>)  </li>

  <li> Seminar: <a href=" http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/ReadingGroup/ReadingGroup.htm"> Reading Group Algorithms</a> (<a href=" http://people.mpi-inf.mpg.de/~mehlhorn/">Kurt Mehlhorn</a>, <a href=" http://people.mpi-inf.mpg.de/~kbringma/">Karl Bringmann</a>)  </li>

  <li> Seminar:   <a href=" http://resources.mpi-inf.mpg.de/departments/d1/teaching/ws12/SocialChoiceTheory">Social Choice Theory</a> (<a href=" http://people.mpi-inf.mpg.de/~vanstee/">Rob van Stee</a>, <a href=" http://people.mpi-inf.mpg.de/~crizkall/">Christine Rizkallah</a>) </li></ul>

<h2>Summer 2012</h2>
<ul>
   <li> Lecture
    <a href="teaching/ss12/OPT/">Optimization</a> (<a href="https://people.mpi-inf.mpg.de/~rspoehel/" target="_blank">Reto Sp&ouml;hel</a>, <a href="https://people.mpi-inf.mpg.de/~vanstee/" target="_blank">Rob van Stee</a>)
    </li>
<li> Lecture
   <a href="teaching/ss12/alg_eng/">Algorithm Engineering </a>(<a href="https://people.mpi-inf.mpg.de/~eric/" target="_blank">Eric Berberich</a>, <a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~gawry/" target="_blank">Paweł Gawrychowski</a>)
    </li>
<li> Lecture
   <a href ="teaching/ss12/AdvancedGraphAlgorithms/">Advanced Graph Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~duanran/" target="_blank">Ran Duan</a>, <a href="https://people.mpi-inf.mpg.de/~jeschmid/" target="_blank">Jens M. Schmidt</a>, <a href="https://people.mpi-inf.mpg.de/~wahl/" target="_blank">Magnus Wahlström</a>)
    </li>
    <li> Lecture
  <a href ="teaching/ss12/CompNumberTheory/">Computational Number Theory and Algebra</a> (<a href="http://www-cc.cs.uni-saarland.de/mblaeser/" target="_blank">Markus Bl&auml;ser</a>, <a href="https://people.mpi-inf.mpg.de/~csaha/" target="_blank">Chandan Saha</a>)
</li>
 <li> Lecture
           <a href= "teaching/ss12/ExpTimeAlgo/">Exponential-Time Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~mmnich/" target="_blank">Matthias Mnich</a>)
</li>
    <li> Lecture
             <a href="teaching/ss12/basicmath/">Basic Mathematical Techniques for Computer Scientists</a> (<a href="https://people.mpi-inf.mpg.de/~koetzing/" target="_blank">Timo Kötzing</a>, <a href="https://people.mpi-inf.mpg.de/~xperez/" target="_blank">Xavier P&eacute;rez Gim&eacute;nez</a>, <a href="https://people.mpi-inf.mpg.de/~csaha/" target="_blank">Chandan Saha</a>, <a href="https://people.mpi-inf.mpg.de/~sauerwal/" target="_blank">Thomas Sauerwald</a>, <a href="https://people.mpi-inf.mpg.de/~rspoehel/" target="_blank">Reto Sp&ouml;hel</a>, <a href="https://people.mpi-inf.mpg.de/~jeschmid/" target="_blank">Jens M. Schmidt</a>, <a href="https://people.mpi-inf.mpg.de/~winzen/" target="_blank">Carola Winzen</a>)
</li>
 <li> Lecture
           <a href="teaching/ss12/learning/">Limits of Computational Learning</a> (<a href="https://people.mpi-inf.mpg.de/~koetzing/" target="_blank">Timo Kötzing</a>)
</li>
<li> Seminar:
    <a href="teaching/ss12/ReadingGroup/ReadingGroup.htm">Reading Group Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~winzen/" target="_blank">Carola Winzen</a>)
    </li>
   </ul>

<h2>Winter 2011/2012</h2>
<ul>
     <li> Lecture
        <a href="teaching/ws11/ct/"> Computational Thinking</a>  (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~kpanagio/" target="_blank">Konstantinos Panagiotou</a>)
   </li>
<li> Lecture
            <a href="teaching/ws11/SGT/index.html">Spectral Graph Theory</a> (<a href="https://people.mpi-inf.mpg.de/~hsun/" target="_blank">He Sun</a>, <a href="https://people.mpi-inf.mpg.de/~sauerwal/" target="_blank">Thomas Sauerwald</a>)
                    </li>
<li> Lecture
            <a href="teaching/ws11/armcs/">Advanced Randomized Methods in Computer Science</a> (<a href="https://people.mpi-inf.mpg.de/~doerr/" target="_blank">Benjamin Doerr</a>, <a href="https://people.mpi-inf.mpg.de/~kpanagio/" target="_blank">Konstantinos Panagiotou</a>, <a href="https://people.mpi-inf.mpg.de/~sauerwal/" target="_blank">Thomas Sauerwald</a>, <a href="https://people.mpi-inf.mpg.de/~rspoehel/" target="_blank">Reto Sp&ouml;hel</a>)
 </li>
<li> Lecture
           <a href="teaching/ws11/grads/">Grundz&uuml;ge Algorithmen und Datenstrukturen</a> (<a href="https://people.mpi-inf.mpg.de/~doerr/" target="_blank">Benjamin Doerr</a>, <a href="https://people.mpi-inf.mpg.de/~rspoehel/" target="_blank">Reto Sp&ouml;hel</a>)</li>

<li> Lecture
           <a href="teaching/ws11/OPTII/OPTII.htm">Optimization II</a> (<a href="https://people.mpi-inf.mpg.de/~elbassio/" target="_blank">Khaled Elbassioni</a>, <a href="https://people.mpi-inf.mpg.de/~saurabh/" target="_blank">Saurabh Ray</a>)</li>


<li> Seminar:
        <a href="teaching/ws11/multicore/">Multi-Core Programming Lab on Swarm Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~tfried/" target="_blank">Tobias Friedrich</a>, <a href="https://people.mpi-inf.mpg.de/~koetzing/" target="_blank">Timo Kötzing</a>)
        </li>
  <li> Seminar:
     <a href="teaching/ws11/ReadingGroup/ReadingGroup.htm"> Reading Group Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~winzen/" target="_blank">Carola Winzen</a>)
 </li>
<li> Seminar:
       <a href="teaching/ws11/AGT/">Algorithmic Game Theory</a> (<a href="https://people.mpi-inf.mpg.de/~vanstee/" target="_blank">Rob van Stee</a>)
        </li>

   </ul>

<h2>Summer 2011</h2>
<ul>
 <li> Lecture
	 <a href= "teaching/ss11/AGT/">Algorithmic Game Theory</a>  (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~vanstee/" target="_blank">Rob van Stee</a>)
    </li>
 <li> Lecture
	 <a href= "teaching/ss11/ComputerAlgebra/">Computer Algebra</a>  (<a href="https://people.mpi-inf.mpg.de/~msagralo/" target="_blank">Michael Sagraloff</a>)
    </li>   
     <li> Lecture
	 <a href="teaching/ss11/OPT/">Optimization</a>  (<a href="https://people.mpi-inf.mpg.de/~anke/" target="_blank">Anke van Zuylen</a>, <a href="https://people.mpi-inf.mpg.de/~elbassio/" target="_blank">Khaled Elbassioni</a>)
    </li>
    <li> Lecture
	<a href="teaching/ss11/graph_theory/">Graph Theory</a> (<a href="https://people.mpi-inf.mpg.de/~doerr/" target="_blank">Benjamin Doerr</a>, <a href="https://people.mpi-inf.mpg.de/~hermelin/" target="_blank">Danny Hermelin</a>, <a href="https://people.mpi-inf.mpg.de/~rspoehel/" target="_blank">Reto Sp&ouml;hel</a>)
</li>
    <li> Lecture
            <a href="teaching/ss11/ProbMethod/">The Probabilistic Method and Randomised Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~janegao/" target="_blank">Jane Gao</a>, <a href="https://people.mpi-inf.mpg.de/~xperez/" target="_blank">Xavier P&eacute;rez Gim&eacute;nez</a>, <a href="https://people.mpi-inf.mpg.de/~sauerwal/" target="_blank">Thomas Sauerwald</a>)
	            </li>
 <li> Lecture
           <a href="teaching/ss11/TopologicalMethods/"> Topological Methods in Geometry</a> (<a href="https://people.mpi-inf.mpg.de/~saurabh/" target="_blank">Saurabh Ray</a>, <a href="http:/sma.epfl.ch/~moustafa/index.html" target="_blank">Nabil H. Mustafa</a>)
	            </li>
		    
   <li> Seminar:
        <a href="teaching/ss11/swarm/">Theoretical Foundations of Swarm Intelligence</a> (<a href="https://people.mpi-inf.mpg.de/~tfried/" target="_blank">Tobias Friedrich</a>, <a href="https://people.mpi-inf.mpg.de/~koetzing/" target="_blank">Timo K&ouml;tzing</a>)
   	</li>
  <li> Seminar:
       <a href="teaching/ss11/ReadingGroup/ReadingGroup.htm">Reading Group</a> (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~winzen/" target="_blank">Carola Winzen</a>)
   	</li>
   </ul>

<h2>Winter 2010/2011</h2>
<ul>
     <li> Lecture
	<a href="teaching/ws10/AO/"> Optimization II- Approximation and Online algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~villars/" target="_blank">Chien-Chung Huang</a>, <a href="https://people.mpi-inf.mpg.de/~vanstee/" target="_blank">Rob van Stee</a>)
        </li>
    <li> Lecture
	<a href="teaching/ws10/models_of_computation">Models of Computation, an Algorithmic Perspective</a> (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~kpanagio/" target="_blank">Konstantinos Panagiotou</a>, <a href="https://people.mpi-inf.mpg.de/~rspoehel/" target="_blank">Reto Sp&ouml;hel</a>)
</li>
    <li> Lecture
            <a href="teaching/ws10/EG/WS10.html">Expander Graphs in CS</a> (<a href="https://people.mpi-inf.mpg.de/~hsun/" target="_blank">He Sun</a>)
		    </li>
   <li> Seminar:
        Randomized Algorithms (<a href="https://people.mpi-inf.mpg.de/~tfried/" target="_blank">Tobias Friedrich</a>, <a href="https://people.mpi-inf.mpg.de/~sauerwal/" target="_blank">Thomas Sauerwald</a>)
   	</li>
<li> Seminar:
        Parameterized Algorithms and Complexity (<a href="http://people.mmci.uni-saarland.de/~jguo/" target="_blank">Jiong Guo</a>, <a href="https://people.mpi-inf.mpg.de/~hermelin/" target="_blank">Danny Hermelin</a>, <a href="https://people.mpi-inf.mpg.de/~wahl/" target="_blank">Magnus Wahlstr&ouml;m</a>)
   	</li>

   </ul>

<h2>Summer 2010</h2>

<ul>
   <li> Lecture
	<a href="teaching/ss10/opt/index.html">Optimization</a> (<a href="http://www.mpi-inf.mpg.de/~nmegow/">Nicole Megow</a>,
     <a href="http://www.mpi-inf.mpg.de/~mehlhorn/">Kurt Mehlhorn</a>, <a href="http://www.mpi-inf.mpg.de/~jmestre">Julian Mestre</a>)
        </li>
   <li> Lecture
	<a href="teaching/ss10/MFI2/index.html">Mathematik f&uuml;r Informatiker II</a> (<a href="http://www.mpi-inf.mpg.de/~doerr/">Benjamin Doerr</a>)
        </li>
      <li> Lecture
<a href="teaching/ss10/Lecture_GameTheory/index.html">Algorithmic Game Theory</a>  (<a href="http://www.mpi-inf.mpg.de/~bonifaci/" target="_blank">Vincenzo Bonifaci</a>, <a href="http://www.mpi-inf.mpg.de/~elbassio/" target="_blank">Khaled Elbassioni</a>, <a href="http://www.mpi-inf.mpg.de/~angelina/">Angelina Vidali</a>)
        </li>
 <li> Lecture
<a href="teaching/ss10/AlgoRand/index.html">Algorithms and Randomization</a> (<a href="http://www.mpi-inf.mpg.de/~chinmoy" target="_blank">Chinmoy Dutta</a>, <a href="http://www.mpi-inf.mpg.de/~fountoul/" target="_blank">Nikolaos Fountoulakis</a>, <a href="http://www.mpi-inf.mpg.de/~ahuber/">Anna Huber</a>)
        </li>
   <li> Seminar:
        Modern Topics in Algorithmics (NN)
   	</li>
   	<li> Seminar
	<a href="teaching/ss10/Seminar_CGGC/index.html">Computational Geometry and Geometric Computing</a> (<a href="https://people.mpi-inf.mpg.de/~eric/" target="_blank">Eric Berberich</a>, <a href="https://people.mpi-inf.mpg.de/~msagralo/" target="_blank">Michael Sagraloff</a>, <a href="https://people.mpi-inf.mpg.de/~bgalehou/" target="_blank">Ben Galehouse</a>)
        </li>
</ul>

<h2>Winter 2009/2010</h2>
<ul>
   <li> Lecture
        Algorithms and Data Structures (<a href="https://people.mpi-inf.mpg.de/~tfried/" target="_blank">Tobias Friedrich</a>, <a href="https://people.mpi-inf.mpg.de/~fne/" target="_blank">Frank Neumann</a>, <a href="https://people.mpi-inf.mpg.de/~vanstee/" target="_blank">Rob van Stee</a>)
        </li>
   <li> Lecture
	<a href="teaching/ws09_10/CGGC/index.html">Computational Geometry and Geometric Computing</a> (<a href="https://people.mpi-inf.mpg.de/~eric/" target="_blank">Eric Berberich</a>, <a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="https://people.mpi-inf.mpg.de/~msagralo/" target="_blank">Michael Sagraloff</a>)
        </li>
   <li> Lecture
	<a href="teaching/ws09_10/Opt2/index.html">Optimization II</a> (<a href="http://www.mpi-inf.mpg.de/~jmestre/" target="_blank">Juli&aacute;n Mestre</a>, <a href="http://www.mpi-inf.mpg.de/~elbassio/" target="_blank">Khaled Elbassioni</a>)
        </li>
   <li> Seminar:
        <a href="http://www.mpi-inf.mpg.de/~doerr/teaching/randomizedsearchheuristics.html">Theory of Randomized Search Heuristics</a> (<a href="https://people.mpi-inf.mpg.de/~doerr/" target="_blank">Benjamin Doerr</a>, <a href="https://people.mpi-inf.mpg.de/~kpanagio/" target="_blank">Kosta Panagiotou</a>)
   	</li>
   <li> Seminar:
        <a href="http://www.mpi-inf.mpg.de/~doerr/teaching/proseminarWS0910.html">Theory of Algorithms</a> (<a href="https://people.mpi-inf.mpg.de/~doerr/" target="_blank">Benjamin Doerr</a>, Carola Winzen)
   	</li>
   <li> Seminar:
        <a href="http://www.mpi-inf.mpg.de/~villars/GTS/course.html">Modern Topics in Algorithmics: Game Theory</a> (<a href="https://people.mpi-inf.mpg.de/~mehlhorn/" target="_blank">Kurt Mehlhorn</a>, <a href="http://www.mpi-inf.mpg.de/~villars/" target="_blank">Chien-Chung Huang</a>)
   	</li>
</ul>

<h2>Summer 2009</h2>

<ul>
   <li> Lecture
        Optimization
        </li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~villars/cdm/course.html">
        Computational Discrete Mathematics
	</a> 
	   </li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~saurabh/DiscreteGeometry/DiscreteGeometry.html">
        Discrete Geometry
	</a> 
	 </li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~mehlhorn/SelectedTopics/SelectedTopics.html">
        Selected Topics in Algorithms
	</a>
	  </li>
   <li> Lecture
        The Probabilistic Method
        </li>
   <li> Seminar:
        Optimization under Uncertainty: Stochastic and Online Models
        </li>
</ul> 


<h2>Winter 2008/2009</h2>
<ul>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~doerr/teaching/randomizedalgorithms0809.html">
        <!-- Just a permissions issue.  Should be online soon. -->
        Randomized Algorithms (Selected Topics)
        </a></li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~rraman/approxCoursePage.html">
        Approximation Algorithms
        </a></li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~msagralo/teaching/NonlinearGeo.mht">
        Nonlinear Computational Geometry
        </a></li>
   <li> Seminar:
        Optimization of Submodular Functions and Their Applications
        </li>
</ul>


<h2>Summer 2008</h2>

<ul>
   <li> Lecture
        Tropical Geometry and Algebraic Statistics
        </li>
   <li> Lecture
        Internet Economics
        </li>
    <li> Lecture
        <a href="/departments/d1/teaching/ss08/opt08">	Optimization
        </a></li>
    <li>Lecture:
        <a href="https://people.mpi-inf.mpg.de/~mehlhorn/DatAlg2008/DatAlg2008.html">
        Algorithms and Data Structures</a></li>
   <li> Seminar:
        Bio-inspired Computation
        </li>
</ul>

<h2>Winter 2007/2008</h2>
<ul>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~doerr/teaching/algodat0708.html">
        <!-- Just a permissions issue.  Should be online soon. -->
        Algorithms and Datastructures
        </a></li>
</ul>

<h2>Summer 2007</h2>

<ul>
   <li> Lecture
        Optimization
        </li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~funke/Courses/machinelearning/">
        Machine Learning
        </a></li>
</ul>


<h2>Winter 2006/2007</h2>

<ul>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~doerr/teaching/evolutionary_algorithms.html">
        <!-- Just a permissions issue.  Should be online soon. -->
        Evolutionary Algorithms
        </a></li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~doerr/teaching/graphtheory.html">
        <!-- Just a permissions issue.  Should be online soon. -->
        Graph Theory
        </a></li>
   <li> Seminar:
        <a href="https://people.mpi-inf.mpg.de/~jgiesen/tch/sem06/sem06.html">
        Computational Topology
        </a></li>
   <li> Seminar:
        Searching with Suffix Arrays
        </li>
</ul>


<h2>Summer 2006</h2>

<ul>
   <li> Lecture
        <a href="/departments/d1/teaching/ss06/alds06/">
        Algorithms for Large Data Sets
        </a></li>
   <li> Lecture
        <a href="/departments/d1/teaching/ss06/opt06">

        Optimization
        </a></li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~funke/Courses/compgeom/">
        Computational Geometry
        </a></li>
   <li> Lecture
        Advanced Data Structures
        </li>
   <li> Lecture
        Combinatorial Geometry
        </li>
   <li> Lecture
        Einf&uuml;hrung in die Informatik f&uuml;r H&ouml;rer aller Fakult&auml;ten I
        </li>
   <li> Seminar:
        <a href="https://people.mpi-inf.mpg.de/~doerr/teaching/liargamesseminar06.html">
        <!-- Just a permissions issue.  Should be online soon. -->
       	Liar Games and Noisy Channels
        </a></li>
</ul>

<h2>Winter 2005/2006</h2>

<ul>
   <li> Lecture 
        Approximation Algorithms
        </li>
   <li> Lecture 
        <a href="https://people.mpi-inf.mpg.de/~doerr/teaching/discrepancy0506.html">
        <!-- Just a permissions issue.  Should be online soon. -->
        Discrepancy Theory
        </a></li>
   <li> Lecture 
        <a href="/departments/d1/teaching/ws05_06/da05/">
        Datastructures and Algorithms
        </a></li>
   <li> Lecture 
        <a href="https://people.mpi-inf.mpg.de/~funke/Courses/randalg/">
        Randomized Algorithms
        </a></li>
   <li> Seminar:
        Computational and Algebraic Geometry
        </li>
   <li> Seminar:
        Information Retrieval
        </li>
</ul>

<h2>Summer 2005</h2>

<ul>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~mehlhorn/AlgorithmEngineering/AlgorithmEngineering.html">
        Algorithm Engineering
        </a></li>
   <li> Lecture
        <a href="/departments/d1/teaching/ss05/opt05/index.html">
        Optimization
        </a></li>
   <li> Lecture
        <a href="/departments/d1/teaching/ss05/approx05">
        Online- and Approximation Algorithms
        </a></li>
   <li> Seminar:
        <a href="https://people.mpi-inf.mpg.de/~sschmitt/proseminar-05.html">
        Geometrische Algorithmen
        </a></li>
</ul>

<h2>Winter 2004/2005</h2>

<ul>
   <li> Lecture 
        Exakte und Effiziente Algorithmen f&uuml;r Kurven und Fl&auml;chen
        </li>
   <li> Lecture 
        Data Structures and Algorithms</li>
   <li> Seminar:
        <a href="https://people.mpi-inf.mpg.de/~kettner/courses/rounding_seminar_04/">
        Geometrisches Runden
        </a></li>
   <li> Seminar:
        Advanced Topics in Information Retrieval
        </li>
</ul>

<h2>Summer 2004</h2>

<ul>
   <li> Lecture
        Optimization</li>
   <li> Lecture
        <a href="https://people.mpi-inf.mpg.de/~althaus/vor_large_opt.html">
        Large Scale Optimization</a></li>
   <li> Lecture
        Algorithmic Aspects of Wireless Networking</li>
   <li> Lecture
        Algorithmic Graph Theory</li>
   <li> Proseminar:
        Das BUCH der Beweise - Proofs from THE BOOK</li>
   <li> Seminar:
        Das BUCH der Beweise - Proofs from THE BOOK</li>
   <li> Proseminar:
        <a href="https://people.mpi-inf.mpg.de/~sschmitt/proseminar-04.html">
        Theorie und Praxis geometrischer Algorithmen</a></li>
   <li> Seminar:
        <a href="https://people.mpi-inf.mpg.de/~sschmitt/seminar-04.html">
        Theorie und Praxis geometrischer Algorithmen</a></li>
</ul>

<h2> Winter 2003/2004</h2>

<ul>
   <li> Vorlesung: <a href="teaching/ws03_04/theoinf/index.html">
        Theoretische Informatik</a></li>
   <li> Vorlesung: <a href="teaching/sanders/courses/algdat03/index.html">
        Data Structures and Algorithms</a></li>
   <li> Vorlesung:
        Effective Computational Geometry for Curves and Surfaces</li>
   <li> Vorlesung: <a href="https://people.mpi-inf.mpg.de/~umeyer/lectures/ws03.html">
        Parallel and Distributed Algorithms</a></li>
   <li> Seminar: <a href="https://people.mpi-inf.mpg.de/~althaus/sem_ipco.html">
        Integer Programming and Combinatorial Optimization</a></li>
</ul>

<h2> Sommer 2003</h2>

<ul>
   <li> Vorlesung: <a href="https://people.mpi-inf.mpg.de/~kettner/courses/lib_design_03/">
        Algorithm Library Design </a></li>
   <li> Vorlesung:
        Optimierung</li>
   <li> Vorlesung:
        Network Flows</li>
   <li> Seminar: <a href="https://people.mpi-inf.mpg.de/~umeyer/seminars/ss03.html">
        Parallel and External Memory Graph Algorithms </a></li>
</ul>

<h2> Winter 2002/2003 </h2>

<ul>
   <li>
    Vorlesung:
    <a href="teaching/sanders/courses/randalg/index.html">
    Randomized Algorithms
    </a></li>
   <li>
    Mini-course:
    <a href="https://people.mpi-inf.mpg.de/~mehlhorn/SelectedTopics02-03/SelectedTopics.html">
    Selected Topics in Algorithms
    </a></li>
   <li>
    Seminar:
    Approximation Algorithms
   </li>
</ul>

<h2> Sommer 2002 </h2>

<ul>
   <li>
    Vorlesung:
    Computational Geometry
    </li>
   <li>
    Vorlesung:
    <a href="teaching/sanders/courses/paralg02/index.html">
    Parallel and Distributed Algorithms
    </a></li>
   <li>
    Mini-course:
    <a href="https://people.mpi-inf.mpg.de/~mehlhorn/SelectedTopics02/SelectedTopics.html">
    Selected Topics in Algorithms
    </a></li>
</ul>

<h2> Winter 2001/2002 </h2>

<ul>
   <li>
    Vorlesung:
    <a href="https://people.mpi-inf.mpg.de/~mehlhorn/ECG/ECG-Vorlesung.html">
    Theory and Practice of Implementing Geometric Algorithms
    </a></li>
   <li>
    Vorlesung:
    <a href="teaching/sanders/courses/algdat02/index.html">
    Data Structures and Algorithms
    </a></li>
   <li>
    Vorlesung:
    Complexity Theory</li>
   <li>
    Seminar:
    Algorithmic Aspects of Networks
    </li>
   <li>
    Seminar:
    Theory and Practics of Implementing Geometric Algorithms
    </li>
</ul>

<h2> Sommer 2001 </h2>

<ul>
  <li>
    Seminar:
    <a href="teaching/sanders/courses/semmassive/index.html">
    Algorithmen f&uuml;r Gro&szlig;e Datenmengen
    </a></li>
   <li>
    Vorlesung:
    Randomisierte Algorithmen</li>
   <li>
    Vorlesung:
    <a href="https://people.mpi-inf.mpg.de/~mehlhorn/Optimization.html">
    Optimization:  Linear and Integer Programming and Approximation Algs
    </a></li>
</ul>

<h2> Winter 2000/2001 </h2>

<ul>
  <li>
    Vorlesung:
    Grundlagen zu Algorithmen und Datenstrukturen</li>
  <li>
    Vorlesung:
    <a href = "teaching/sanders/courses/algdat2/index.html">
    Algorithmen und Datenstrukturen</a></li>
  <li>
    Vorlesung:
    <a href = "https://people.mpi-inf.mpg.de/~umeyer/vorlesung/">
    Advanced Algorithms and Data Structures for Different Models of Computation
</a></li>
  <li>
    Vorlesung:
    Geometry of Mesh Generation</li>
</ul>

<h2> Sommer 2000 </h2>

<ul>
  <li> Vorlesung:
    <a href = "https://people.mpi-inf.mpg.de/~mehlhorn/Informatik5.html">
    Informatik V: Grundlagen von Datenstrukturen u. Algorithmen</a></li>
  <li> Vorlesung:
    Software Design Praktikum</li>
</ul>

<h2> Winter 1999/2000 </h2>

<ul>
  <li>
    Vorlesung:
    Bioinformatik</li>
  <li>
    Vorlesung: <a href = "teaching/ws99_00/praxprog/index.html">
    Praxis des Programmierens</a></li>
  <li>
    Seminar: <a href = "teaching/sanders/courses/algen/">
      Algorithm Engineering</a></li>
  <li>
    Seminar
      Parallel Algorithms</li>
  <li>
    FoPra:
    Anbindung von LEDA an Java</li>
</ul>

<h2> Sommer 1999 </h2>

<ul>
  <li>
    Vorlesung: <a href = "teaching/sanders/courses/paralg/paralg.html">
      Parallel Algorithms</a></li>
</ul>

<h2> Winter 1998/98 </h2>

<ul>
  <li>
    Vorlesung:
    Komplexit&auml;tstheorie</li>
  <li>
    Vorlesung: <a href="teaching/sanders/courses/praxprog/praxprog.html">
    Praxis des Programmierens</a></li>
  <li>
    Vorlesung:
    Datenstrukturen und Algorithmen</li>
  <li>
    FoPra: <a href="https://people.mpi-inf.mpg.de/~stschirr/fopra9899.html">
    Implementierung geometrischer Algorithmen</a></li>
  <li>
    FoPra:
    Algorithmen f&uuml;r gro&szlig;e Datenmengen</li>
  <li>
    FoPra:
    Algorithmen zum Zeichnen von Graphen</li>
</ul>

<h2> Sommer 1998 </h2>

<ul>
  <li>
    Vorlesung: <a href="http://people.mpi-inf.mpg.de/~stschirr/advanced.html">
    Advanced C++</a></li>
  <li>
    Vorlesung: <a href="teaching/sanders/courses/parprog.html">
    Parallele Programmierung</a></li>
  <li>
    Seminar:
    Quantencomputer</li>
  <li>
    Seminar:
    Randomisierte Algorithmen</li>
  <li>
    FoPra:
    Geometrische Algorithmen</li>
  <li>
    FoPra:
    Erl&auml;uterung von Algorithmen mittels Applets</li>
</ul>

<h2> Winter 1997/98 </h2>

<ul>
  <li>
    Vorlesung:
    Datenstructuren und Algorithmen</li>
  <li>
    Vorlesung:
    Computational Molecular Biology</li>
  <li>
    Vorlesung:
    Ganzzahlige Optimierung</li>
  <li>
    FoPra:
    Implementierung von Dynamischen Graphenalgorithmen</li>
</ul>

<h2> Sommer 1997 </h2>

<ul>
  <li>
    Vorlesung:
    Optimierung</li>
  <li>
    Vorlesung:
    Praxis des Programmierens</li>
  <li>
    Vorlesung:
    Parallele Algorithmen</li>
</ul>

<h2> Winter 1996/97 </h2>

<ul>
  <li>
    Vorlesung:
    Praxis des Programmierens</li>
  <li>
    Vorlesung:
    Datenstrukturen und Algorithmen</li>
  <li>
    FoPra:
    Distributed Protocols Implementation and Evaluation</li>
  <li>
    FoPra:
    Geometrische Algorithmen</li>
  <li>
    FoPra:
    Implementierung von parallelen Algorithmen mit Hilfe von PAD</li>
  <li>
    FoPra
    Implementierung von Dynamischen Graphenalgorithmen</li>
</ul>

<h2> Sommer 1996</h2>

<ul>
  <li>
    Vorlesung:
    Informatik 4</li>
</ul>
