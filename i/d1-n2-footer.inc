      <li><a href="https://www.mpi-inf.mpg.de/departments/computer-vision-and-multimodal-computing/" accesskey="2" title="D2: Computer Vision and Multimodal Computing">Computer Vision and Multimodal Computing</a></li>
      <li><a href="https://www.mpi-inf.mpg.de/departments/computational-biology-applied-algorithmics/" accesskey="3" title="D3: Computational Biology &amp; Applied Algorithmics">Computational Biology &amp; Applied Algorithmics</a></li>
      <li><a href="https://www.mpi-inf.mpg.de/departments/computer-graphics/" accesskey="4" title="D4: Computer Graphics">Computer Graphics</a></li>
      <li><a href="https://www.mpi-inf.mpg.de/departments/databases-and-information-systems/" accesskey="5" title="D5: Databases and Information Systems">Databases and Information Systems</a></li>
      <li><a href="/departments/am" accesskey="6" title="Independant Research Group 1: Computational Genomics and Epidemiology">Computational Genomics and Epidemiology</a></li>
      <li><a href="/departments/ontologies" accesskey="7" title="Otto Hahn Research Group: Ontologies" id="fms-homepage">Ontologies</a></li>
      <li><a href="/departments/rg1/index.html" accesskey="r" title="Research Group 1: Automation of Logic">Automation of Logic</a></li>
    </ul>
  </li>
